﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParkingConsoleView
{
    public class ParkingSettings
    {
        private readonly ParkingService _parkingService;
        private static readonly Lazy<ParkingSettings> instance =
            new(() => new ParkingSettings());

        public static ParkingSettings Instance => instance.Value;

        private ParkingSettings()
        {
            _parkingService = new ParkingService();
        }

        public void PrintInfo()
        {
            var s = $"Balance: {_parkingService.GetBalance().ToString(CultureInfo.CurrentCulture)}\n" +
                    $"Capacity: {_parkingService.GetCapacity().ToString()}\n" +
                    $"WithdrawTimer: {Parking.Instance.WithdrawTimer}\n" +
                    $"LogTimer: {Parking.Instance.LogTimer}";
            Console.WriteLine();
            Console.WriteLine("***Info***");
            Console.WriteLine(s);
            Console.WriteLine("***Price***");
            foreach (var d in Parking.Instance.DictionaryPrice)
            {
                Console.WriteLine($"{d.Key}: {d.Value}");
            }
            Console.WriteLine();
        }
        private void SetCapacity()
        {
            Console.Write("Enter capacity: ");
            var c = Console.ReadLine();

            int number;
            bool isInt = int.TryParse(c, out number);

            if (isInt)
                Parking.Instance.Capacity = Convert.ToInt32(c);
            else
                SetCapacity();
        }
        private void SetWithdraw()
        {
            Console.Write("Enter withdrawal time: ");
            var c = Console.ReadLine();
            int number;
            bool isInt = int.TryParse(c, out number);

            if (isInt)
                Parking.Instance.WithdrawTimer = Convert.ToInt32(c);
            else
                SetWithdraw();
        }
        private void SetLogTimer()
        {
            Console.Write("Enter log time: ");
            var c = Console.ReadLine();
            int number;
            bool isInt = int.TryParse(c, out number);

            if (isInt)
                Parking.Instance.LogTimer = Convert.ToInt32(c);
            else
                SetLogTimer();
        }
        private void SetPrice()
        {
            var menu = new List<string>() { "Exit", "PassengerCar", "Truck", "Motorcycle", "Bus" };
            for (int i = 0; i < menu.Count; i++)
            {
                Console.WriteLine($"{i} - {menu[i]}");
            }
            var c = Console.ReadLine();

            switch (c)
            {
                case "0":
                    SetInfo();
                    break;
                case "1":
                    SetPriceType(VehicleType.PassengerCar);
                    PrintInfo();
                    break;
                case "2":
                    SetPriceType(VehicleType.Truck);
                    PrintInfo();
                    break;
                case "3":
                    SetPriceType(VehicleType.Motorcycle);
                    PrintInfo();
                    break;
                case "4":
                    SetPriceType(VehicleType.Bus);
                    PrintInfo();
                    break;
                default:
                    SetInfo();
                    break;
            }

        }
        private void SetPriceType(VehicleType type)
        {
            double number;
            var dict = Parking.Instance.DictionaryPrice;
            var v = dict.FirstOrDefault(x => x.Key == type);

            Console.Write("Enter price: ");
            var c = Console.ReadLine();
            bool isDouble = double.TryParse(c, out number);

            if (isDouble)
            {

                dict.Remove(v.Key);
                dict.Add(v.Key, number);
            }

            else
                SetPriceType(type);

        }
        public void SetInfo()
        {
            Console.WriteLine("***Edit***");
            var menu = new List<string>() { "Exit", "Capacity", "Withdrawal time", "Log time", "Set price", "Info" };

            for (int i = 0; i < menu.Count; i++)
            {
                Console.WriteLine($"{i} - {menu[i]}");
            }
            var c = Console.ReadLine();

            switch (c)
            {
                case "0":
                    PrintInfo();
                    break;
                case "1":
                    SetCapacity();
                    SetInfo();
                    break;
                case "2":
                    SetWithdraw();
                    SetInfo();
                    break;
                case "3":
                    SetLogTimer();
                    SetInfo();
                    break;
                case "4":
                    SetPrice();
                    SetInfo();
                    break;
                case "5":
                    PrintInfo();
                    SetInfo();
                    break;
                default:
                    SetInfo();
                    break;
            }

        }
    }
}
