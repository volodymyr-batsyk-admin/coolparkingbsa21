﻿using System;
using System.Collections.Generic;

namespace CoolParkingConsoleView
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowMenu();
        }

        public static void ShowMenu()
        {
            Console.WriteLine("***Menu***");
            var menu = new List<string>() { "Exit", "Parking", "Settings" };

            for (int i = 0; i < menu.Count; i++)
            {
                Console.WriteLine($"{i} - {menu[i]}");
            }
            var c = Console.ReadLine();

            switch (c)
            {
                case "0":
                    break;
                case "1":
                    ParkingMenu.Instance.ShowMenu();
                    break;
                case "2":
                    ParkingSettings.Instance.SetInfo();
                    break;
                default:
                    ShowMenu();
                    break;
            }
        }
    }
}
