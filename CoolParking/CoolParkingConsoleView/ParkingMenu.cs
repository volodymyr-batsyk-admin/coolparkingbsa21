﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParkingConsoleView
{
    class ParkingMenu
    {
        private readonly ParkingService _parkingService;
        private static readonly Lazy<ParkingMenu> instance =
            new(() => new ParkingMenu());

        public static ParkingMenu Instance => instance.Value;

        private ParkingMenu()
        {
            _parkingService = new ParkingService();
        }

        public void ShowMenu()
        {
            Console.WriteLine("***Menu***");
            var menu = new List<string>() { "Exit", "Show all vehicles", "Add vehicle", "Remove vehicle", "Top up balance" };

            for (int i = 0; i < menu.Count; i++)
            {
                Console.WriteLine($"{i} - {menu[i]}");
            }
            var c = Console.ReadLine();

            switch (c)
            {
                case "0":
                    break;
                case "1":
                    ShowVehicles();
                    ShowMenu();
                    break;
                case "2":
                    AddVehicle();
                    ShowMenu();
                    break;
                case "3":
                    DeleteVehicle();
                    ShowMenu();
                    break;
                case "4":
                    TopUpBalance();
                    ShowMenu();
                    break;
                default:
                    ShowMenu();
                    break;
            }
        }

        private void ShowVehicles()
        {
            var vehicles = _parkingService?.GetVehicles().ToList();
            int i = 0;
            if (vehicles != null && vehicles.Count > 0)
            {
                foreach (var vehicle in vehicles)
                {
                    i++;
                    Console.WriteLine($"#{i} - {vehicle.ToString()}");
                }
            }
            else
                Console.WriteLine("No vehicles.");
        }

        #region Add vehicle
        private void AddVehicle()
        {
            Console.WriteLine("***Add vehicle***");
            var type = AddTypeVehicle();
            var balance = AddBalance();

            Vehicle vehicle = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), type, balance);
            _parkingService.AddVehicle(vehicle);
        }
        private VehicleType AddTypeVehicle()
        {
            var type = VehicleType.PassengerCar;

            Console.WriteLine("Choose type");
            var typeMenu = new List<string>() { "Exit", "PassengerCar", "Bus", "Truck", "Motorcycle" };

            for (int i = 0; i < typeMenu.Count; i++)
            {
                Console.WriteLine($"{i} - {typeMenu[i]}");
            }
            var c = Console.ReadLine();

            switch (c)
            {
                case "0":
                    break;
                case "1":
                    type = VehicleType.PassengerCar;
                    break;
                case "2":
                    type = VehicleType.Bus;
                    break;
                case "3":
                    type = VehicleType.Truck;
                    break;
                case "4":
                    type = VehicleType.Motorcycle;
                    break;
                default:
                    AddTypeVehicle();
                    break;
            }

            return type;
        }
        private decimal AddBalance()
        {
            decimal balance = 0;
            Console.Write("Balance: ");
            var c = Console.ReadLine();
            bool isDecimal = decimal.TryParse(c, out balance);

            if (isDecimal)
                return balance;
            else
                AddBalance();
            return balance;
        }
        #endregion

        private void TopUpBalance()
        {
            decimal balance = 0;
            var vehicle = FindVehicle();
            Console.WriteLine($"Current balance: {vehicle.Balance}");
            Console.Write("Tip up: ");
            var c = Console.ReadLine();
            bool isDecimal = decimal.TryParse(c, out balance);
            if (isDecimal)
                _parkingService.TopUpVehicle(vehicle.Id, balance);
            else
                AddBalance();
        }

        private Vehicle FindVehicle()
        {
            Vehicle vehicle = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Bus, 0);
            ShowVehicles();
            int idx;
            Console.WriteLine("Enter index vehicles");
            var c = Console.ReadLine();
            bool isInt = int.TryParse(c, out idx);

            if (isInt)
            {
                vehicle = _parkingService.GetVehicles().ToArray()[idx - 1];
            }
            else
                FindVehicle();
            return vehicle;
        }

        private void DeleteVehicle()
        {
            int num = 0;
            var vehicle = FindVehicle();

            if (vehicle != null)
                _parkingService.RemoveVehicle(vehicle.Id);
        }

    }
}
