﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService: ILogService
    {

        public string LogPath { get; }
        public string Text { get; set; }

        public LogService( string logPath )
        {
            LogPath = logPath;
            Read();
        }
        public void Write(string logInfo)
        {
            Text += logInfo;

            using (var writer = new StreamWriter(LogPath, false))
            {
                writer.WriteLine(Text);
                writer.WriteLine();
            }
        }

        public void Save()
        {
            using (var writer = new StreamWriter(LogPath,false))
            {
                writer.WriteLine(Text);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                Text = "";
            }
            else
            {
                using (var reader = new StreamReader(LogPath))
                {
                    Text = reader.ReadToEnd();
                }
            }

            return Text;
        }
    }
}
