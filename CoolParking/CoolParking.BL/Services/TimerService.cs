﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService: ITimerService
    {
        private Timer Timer { get; set; }
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }

        public TimerService(double interval)
        {
            Interval = interval;
            Timer = new Timer();
            Timer.AutoReset = true;
        }
        public void Start()
        {
            Timer.Enabled = true;
            Timer.Interval = Interval;
            Elapsed += TimerService_Elapsed;

        }

        private void TimerService_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("End timer");
        }

        public void Stop()
        {
            Timer.Enabled = false;
        }

        public void Dispose()
        {
            Timer.Dispose();
        }
    }
}

