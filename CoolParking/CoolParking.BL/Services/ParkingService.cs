﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {

        public ParkingService()
        {
        }
        public void Dispose()
        {
            Console.WriteLine("Dispose");
        }

        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Instance.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Instance.FreePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(Parking.Instance.Cars);

        public void AddVehicle(Vehicle vehicle)
        {
            var list = Parking.Instance.Cars;

            var v = list.FirstOrDefault(x => x.Id == vehicle.Id);

            if (v != null && v.Id.Equals(vehicle.Id) )
                throw new ArgumentException("The vehicle already exists");

            list.Add(vehicle);
            Parking.Instance.FreePlaces--;

        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = Parking.Instance.Cars.Find(x => x.Id == vehicleId);
            if (vehicle != null)
            {
                Parking.Instance.Cars.Remove(vehicle);
                Parking.Instance.FreePlaces++;
            }
            else
            {
                throw new ArgumentException("Vehicle is not exist");
            }

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = Parking.Instance.Cars.Find(x => x.Id == vehicleId);
            if (vehicle != null)
            {
                if (sum < 0)
                {
                    throw new ArgumentException("Negative sum");
                }

                vehicle.Balance += sum;
            }
            else
            {
                throw new ArgumentException("Vehicle is not exist");
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new System.NotImplementedException();
        }

        public string ReadFromLog()
        {
            throw new System.NotImplementedException();
        }
    }
}