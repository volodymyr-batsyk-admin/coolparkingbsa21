﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public  class Parking
    {
        private static readonly Lazy<Parking> instance =
            new(()=> new Parking());

        public decimal Balance { get; set; }
        public int Capacity { get; set; }
        public int FreePlaces { get; set; }
        public int WithdrawTimer { get; set; }
        public int LogTimer { get; set; }

        public static Parking Instance => instance.Value;
        public List<Vehicle> Cars { get; set; }
        public Dictionary<VehicleType, double> DictionaryPrice { get; set; }
        private Parking()
        {
            Cars = new List<Vehicle>();
            Balance = 0;
            Capacity = 10;
            FreePlaces = Capacity;
            WithdrawTimer = 5;
            LogTimer = 60;
            DictionaryPrice = new Dictionary<VehicleType, double>();
            SetPrice(DictionaryPrice);
        }

        private void SetPrice(Dictionary<VehicleType, double> dictionary)
        {
            dictionary.Add(VehicleType.Bus, 3.5);
            dictionary.Add(VehicleType.Motorcycle, 1);
            dictionary.Add(VehicleType.PassengerCar, 2);
            dictionary.Add(VehicleType.Truck, 5);
        }
    }
}