﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (id.Length != GenerateRandomRegistrationPlateNumber().Length)
                throw new ArgumentException("Wrong arguments");
            if (balance < 0)
                throw new ArgumentException("Negative balance");

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public override string ToString() => $"Id:{Id} | Balance:{Balance}\t| Type:{VehicleType}";

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var x = new char[4];
            var y = "";
            var rnd = new Random();
            for (var i = 0; i < x.Length; i++)
            {
                x[i] = (char)rnd.Next(65, 90);
                y += rnd.Next(0, 9);
            }
            return $"{x[0]}{x[1]}-{y}-{x[2]}{x[3]}";
        }
    }
}